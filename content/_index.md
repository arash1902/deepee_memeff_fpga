## Memory-Efficient FPGA-based CNN Acceleration: a Toolflow for Mapping Pruned CNNs on FPGAs

This project targets "Memory-Efficient FPGA-based CNN Acceleration: a Toolflow for Mapping Pruned CNNs on FPGAs". The goal is to automatically generate a synthesizable accelerator for a given (CNN, FPGA) pair from a high-level specification of the model.
The details of our work can be found in the [report page.](https://arash1902.gitlab.io/deepee_memeff_fpga/page/report/)

 <!---The details and results of the pruning flow can be found in the [Pruning](https://arash1902.gitlab.io/deepee_memeff_fpga/page/pruning/) menu. The utilized method for FPGA mapping flow, and obtained results, are elaborated on in the [FPGA mapping](https://arash1902.gitlab.io/deepee_memeff_fpga/page/fpga/) menu. Lastly, The future directions and steps that need to be followed for this project are pointed out [here](https://arash1902.gitlab.io/deepee_memeff_fpga/page/future/). The ultimate goal of this project is automatically generating a synthesizable accelerator for a given (CNN, FPGA) pair from a high-level specification. <!---
 
 

