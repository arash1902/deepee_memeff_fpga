---
title: Pruning
comments: false
---

### Pruning
Modern Convolutional neural networks (CNN) typically consist of multiple convolutional and fully connected layers. 
CNNs are computationally and storage intensive due to their large model size. Pruning the weights of the network is one way to reduce the computational requirements as well as the model size of a CNN with minimal loss of accuracy. 
A systematic approach to pruning is to enforce sparsity among the weights by solving a constrained optimization problem during the training of the CNN. 
We will consider alternate direction method of multipliers (ADMM) based approach for pruning the weights of CNN. The detailed describtion of this method is explained [here](https://dl.acm.org/citation.cfm?id=3304076). We briefly describe this approach.


Consider a \\(N\\)-layer CNN with \\(N_{conv}\\) convolutional layers and \\(N_{full}\\) fully connected layers. 
The weights and biases of \\(i^{th}\\) layer is denoted by \\(\mathbf{W}_i\\) and \\(\mathbf{b}_i\\), respectively. 
Let the loss function of the CNN be denoted by \\(f(\{\mathbf{W}_i\}_{i=1}^N,\{\mathbf{b}_i\}_{i=1}^N)\\). 
Training of a CNN involves finding the set of weights and biases which minimizes the loss function over the training set. 
However, in order to enforce sparsity among the weights, we will solve the following optimization problem:
![opt image](/images/opti.png)



where \\(\mathcal{S}_i\\) are known sets which will incorporate sparsity among the weights of the neural network. 
Two different types of pruning can be considered:

- **Non-structured pruning:** Here there are no constraint on what weights will be dropped. This is considered in [this paper](https://arxiv.org/abs/1804.03294) where \\(\mathcal{S}_i = \{\mathbf{W}_i: \text{cardinality}(\mathbf{W}_i) \leq l_i \}\\). 
- **Structured Pruning:** Here the weights being dropped have some additional spatial structure. This is considered in [this paper](https://arxiv.org/abs/1807.11091) where three types of sparsity structure can be imposed:
    - **Filter pruning:** Here we zero out some of the filters entirely from the convolutional layer weights. 
    - **Channel pruning:** Here we zero out some of the channels entirely from all the filters of a convolutional layer. 
    - **Shape-wise pruning:** Here we zero out some specific shape from each of the filter of a convolutional layer. 
    
An illustration of structured pruning (specifically filter pruning, channel pruning, and shape-wise pruning) is depicted in the following figure:  
![Pruning_3figs image](/images/pruning_3figs.PNG)

We can convert the above optimization into the following optimization:
![opt image](/images/admm_opti.png)
where \\(g_i( \mathbf{W}_i)= 0\\) if \\(\mathbf{W}_i \in \mathcal{S}_i\\); otherwise \\(g_i( \mathbf{W}_i)= \infty\\).

The above optimization can be solved by following the ADMM technique which decomposes the main problem into two sub-problems and solve them separately and 
update parameters \\(\{\mathbf{W}_i\}\\)'s, \\(\{\mathbf{Z}_i\}\\)'s, and Lagrangian coefficients sequentially. In other words, it first solves the first subproblem with respect 
to \\(\{\mathbf{W}_i\}\\)'s while fixing \\(\{\mathbf{Z}_i\}\\)'s and Lagrangian coefficients. Then, it solves the second sub-problem with respect to \\(\{\mathbf{Z}_i\}\\)'s 
while fixing new values of \\(\{\mathbf{W}_i\}\\)'s obtained in the previous sub-problem, and Lagrangian coefficients. Finally, it updates the Lagrangian coefficients
by having the new values of \\(\{\mathbf{W}_i\}\\)'s and \\(\{\mathbf{Z}_i\}\\)'s.

By considering the above ADMM approach, we can solve the constrained optimization problem so as to systematically train a CNN 
while simultaneously enforcing the sparsity constraints among the weights.




### Results on Pruning
We performed experiments for structural pruning on **LeNet5 network on MNIST** dataset. LeNet5 network consists of 2 convolutional layers and 3 fully connected layers. More details on the convolutional
layers are presented in the following table:

| Conv Layer | No. of filters | No. of channels |
| ------ | ------ |------|
| conv1 | 6 | 5 |
| conv2 | 16 | 5 | 

We focussed only on the structured pruning for this set of experiments and in particular we applied filter pruning for each of the convolutional layers.
We define the pruning ratio as the fraction of the filters which will be removed(pruned) from the network from each convolutional layer.
We set the following setting of pruning ratio:
1. **Setting 1** - Pruning ratio = 0.66 for convolutional layer 1 (i.e. two-third of the filters will be pruned from convolutional layer 1) while Pruning ratio = 14/16 for convolutional layer 2. Thus, we only keep two non-zero filters for each convolutional layer. 

In order to further improve the accuracy, we aimed to consider other potential loss functions. The intuition behind this was to use information-theory related loss
functions such as KL divergence to improve the result compared to cross-entropy case. 
We presented the accuracy of different loss functions: cross entropy, negative log likelihood, and KL divergence. The following table 
lists the test accuracy of the baseline (unpruned) network and the pruned network obtained after the ADMM training:

|  | Cross Entropy (used [here](https://arxiv.org/abs/1807.11091)) | Negative log likelihood (Ours) | Kullback–Leibler divergence (Ours) |
| ------ | ------ | ------ | ------ |
| Baseline | 98.11% | 98.11% | 98.64% |
| Setting 1 | 97.72% | 98.0% | 98.45% |

As one can easily see, the scheme with Kullback–Leibler divergence loss functions considerably improves the accuracy of the prunned network. Also, 
it can be seen that the test accuracy obtained after pruning the network is almost similar to the baseline test, for all loss functions we tried above. 

Next, we conducted experiments for structural pruning on **VGG 11 network on CIFAR-10** dataset. VGG 11 network consists of 8 convolutional layers and 3 fully connected layers. Following are the details of the convolutional
layers:

| Conv Layer | No. of filters | No. of channels |
| ------ | ------ |------|
| conv1 | 64 | 3 |
| conv2 | 128 | 64 | 
| conv3 | 256 | 128 |
| conv4 | 256 | 256 | 
| conv5 | 512 | 256 | 
| conv6 | 512 | 512 | 
| conv7 | 512 | 512 | 
| conv8 | 512 | 512 | 

We focused only on the structured pruning for this case and in particular we did filter pruning for each of the convolutional layers.
We tried the following two settings of pruning ratio:
1. **Setting 1** - Pruning ratio = 0.5 for each convolutional layer i.e. 50% of the filters will be pruned from each convolutional layer.
2. **Setting 2** - Pruning ratio = 0.5 for the first 3 conv layers and Pruning ratio = 0.75 for the remaining 5 conv layers.

We also tried two loss functions: cross entropy and negative log likelihood for both experiments. The following table 
lists the test accuracy of the baseline (unpruned) network and the pruned network obtained after the ADMM training:

|  | Cross Entropy (used [here](https://arxiv.org/abs/1807.11091)) | Negative log likelihood (Ours) |
| ------ | ------ | ------ |
| Baseline | 86.22% | 88.67% |
| Setting 1 | 84.29% | 88.24% |
| Setting 2 | 83.38% | 87.03% |

It can be seen that the test accuracy obtained after pruning the network is almost similar to the baseline test accuracy for both loss functions. In particular, for negative log likelihood loss, the accuracy loss after pruning is only slightly lower compared to the baseline accuracy. Therefore, we again observe that the scheme with negative log likelihood loss functions considerably improves the accuracy of the prunned network compared to using cross entropy loss function.

One interesting observation on the histogram of weights of convolutional layers of VGG setting before and after pruning is presented below. 
The distribution of weights of the first four convolutional layers of VGG11 before pruning are as follows:

![before pruning image](/images/unprune_vgg.png)

However, after performing pruning based on the ADMM optimization technique, the distribution of weights of the first four convolutional layers of VGG11 
will be as follows:

![after pruning image](/images/prune_vgg.png)

As one can easily see, most of of the weights of the convolutonal layers are set to zero.


All our experiments were conducted using PyTorch. the details of training is presented below:
![pipeline image](/images/Screen_Shot_2019-12-05_at_8.55.53_PM.png)
1. **Pre-training** - We first trained the VGG network to establish the baseline test accuracy. We did a training-validation split
by splitting 10% of training data as validation data and trained the network for around 100 epochs.
2. **ADMM-training** - We then re-trained the network using the ADMM based training approach. We initialized the weights of the 
network by using the weights of the pre-trained model. We set \\(\rho = 1.5e^{-3}\\) for each convolutional layer.
We did 5 iterations of ADMM steps and for each ADMM step we trained the network for 20 epochs. 
3. **Masked re-training** - The weights obtained after step 2 do not strictly obey the sparsity constraints. Thus, we enforce the sparsity constraints by forcing a number of weights to zero. In particular, for each layer we compute the frobenius norm of the weights of each filter and keep top k filters (w.r.t frobenius norm) where k is the maximum number of non-zero filters allowed in the layer.
The rest of the filter weights are set to zero. Finally, the network is then re-trained without updating the zeroed out weights in order to improve the accuracy.

