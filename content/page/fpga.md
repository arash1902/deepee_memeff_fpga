### Mapping to FPGA
We have started with a toolflow called [DNNWeaver v2.0](http://prism.sejong.ac.kr/dossa-1/dossa_papers/dossa-1_paper01.pdf) for mapping a given neural network on the target FPGA. DNNWeaver attempts to generate a synthesizable accelerator for a given (DNN, FPGA) pair from a high-level specification. To be more specific, in DNNWeaver v2.0, DNNs are first expressed as graphs, where nodes are operations(layers) like convolution, and edges are tensors. Then, the graph is compiled to generate binary instructions for the FPGA accelerator object which is defined separately using a logic synthesis tool like Xilinx Vivado Design Suite. Then, the compiled DNN graph is run on the FPGA. 
An overview of the toolflow is shown in the figure below:

![DNN overview](/images/DNN_overview.JPG)

In the next section, we first quantify the advantages of structural pruning over non-structural pruning for the hardware accelerator. After that, we will briefly introduce the hardware components of an accelerator, followed by the optimization technique added to the current toolflow for finding a better solution in the design space. 

### Structured vs Non-structured Pruning 
Compressing the data cannot be directly translated to improvements in hardware implementation. One solution is to encode the sparse weights matrices. Encoding the sparse weights provides an architecture implemented on FPGA an opportunity to reduce the amount of data that must be moved throughout the memory hierarchy. It also reduces the data footprint, which allows larger matrices to be held a given size storage structure. There exists encoding schemes that are more suitable for performing efficient operations. two commonly used examples that we will explore during next weeks are listed as below.
1. **Compressed Sparse Row (CSR)**. The sparse matrix is represented using three one-dimensional arrays for the non-zero values, the extents of the rows, and the column indices.
2. **Compressed Sparse Column (CSC)**. The same as the Compressed Sparse Row method except the column indices are compressed and read first before the row indices.
These methods are used for encoding the unstructured weight pruning and need extra hardware for endcoding and decoding data while structured weight pruning can be handled by compiler at no extra hardware cost. In the following, its reason will shortly discussed.

The CONV operations in (one layer of) DNNs are commonly transformed to matrix multiplications by converting weight tensors and feature map tensors to matrices, named general matrix multiplication or GEMM, as shown below.
![GEMM](/images/GEMM.PNG)

filter pruning corresponds to reducing one row while Filter shape (channel) pruning corresponds to reducing one column (columns), and thus is also termed column pruning. The three structured pruning techniques, along with their combinations, will reduce the dimensions in GEMM while maintaining a full matrix format without extra indices for locating non-zero weights.

### Accelerator Components for FPGA-based Implementation of CNNs
CNN accelerator design on FPGA consists of several major components, which are processing elements (PEs), on-chip buffer, external memory, and on-/off-chip interconnect. A PE is the basic computation unit for convolution. All data for processing are stored in the external memory. Due to on-chip resource limitation, data are first cached in on-chip buffers before being fed to PEs. Double buffering technique is used to cover computation time with data transfer time. The on-chip interconnect is dedicated for data communication between PEs and on-chip buffer banks. The implementation of our CNN accelerator is as follows:


![Design Block](/images/Design_Block.png)

### Design Space Exploration for FPGA-based Implementation of CNNs
In this section, we start our optimization from the code below and present our methodology for exploring the design space to achieve an efficient design. 

![4 loops](/images/loop.JPG)

For this purpose, we unrolled some of the for loops showed above. This is called tiling and those added loops called a computational tile, which will be processed before storing the final results on the off-chip memory. Here is an example of this tiling: 

![tile loops](/images/tileloops.JPG)

![tile loops 2](/images/tileloops2.JPG)


### Results on FPGA mapping
Using Xilinx Vivado Design Suite, we were able to synthesize the hardware using the clock frequency of 100MHz without any timing failure. For the target Board we used  Virtex UltraScale+ VCU118 Evaluation Platform. The mapped hardware architecture took around 110,000 LUT blocks, which is around 10% of all LUT resources of the studied Board. A screenshot of the synthesized hardware using Vivado is shown here:

![Vivado Results](/images/vivado.jpg)

We mapped two CNN models, which are LeNet-5 and Tiny YOLO, on our target FPGA Borad. For each of these two CNN models, the results for the inference lateny and the amount of hardware energy consumption during the inference, alongside the number of generated binary instructions using DNNWeaver v2.0 are shown below:

| Model | Latency (ms) | Energy (mJ) | No. of Generated Binary Instructions |
| ------ | ------ |------| ------ |
| LeNet-5 | 1.09 | 9.71 | 257 |
| Tiny YOLO | 6.08 | 54.18 | 1433 |

The energy consumption for each model in the table above is obtained from the power consumption of the synthesized hardware (which was 8.91 W), multiplied by the inference time of that model. Compared to GTX 650Ti, which is a middle-tier GPU, obtained results show an average 3.2X improvement in terms of performance-per-Watt.


