## Memory-Efficient FPGA-based CNN Acceleration: a Toolflow for Mapping Pruned CNNs on FPGAs

### Goal
This project targets "Memory-Efficient FPGA-based CNN Acceleration: a Toolflow for Mapping Pruned CNNs on FPGAs". The goal is to automatically generate a synthesizable accelerator for a given (CNN, FPGA) pair from a high-level specification of the model.

### Motivation
Convolutional Neural Networks (CNNs) currently show state-of-the-art performance in many computer vision applications such as image classification, object recognition, scene labeling, and so forth. The state-of-the-art performance of CNNs usually comes at the cost of relatively huge computational complexities, even during the inference. The real-time evaluation of a CNN for image classification on a live video stream can require billions or trillions of operations per second. While this level of performance can be reached with the most recent Graphics Processing Units (GPUs), there is a simultaneous wish to deploy such solutions on embedded systems such as cars, drones, or even wearable devices. These devices exhibit strict limitations regarding physical size and energy consumption. Future embedded CNNs thus call for small and efficient, yet very powerful computing platforms.

![Inference power limited](/images/Motivation_1.png)

In addition, CNNs usually show redundancies in terms of their parameters (e.g., weights). In other words, dropping a portion of weights from pre-trained networks and retraining the remained architecture can sometimes lead to no or negligible accuracy loss, while causing major benefits in terms of computational latency or energy consumption. This is because excessive accesses to the main memory (e.g. DRAM) can consume significant more energy compared to arithmetic operations, and also contribute considerably to the end-to-end inference latency of the network due to inherent wait times associated with the DRAM access time.

### Proposed Flow
We evaluate a structured pruning appraoch to prune a number of CNN benchamrks. Then, we examine mapping of CNN benchamrks on Field-Programmable Gate Arrays (FPGAs). FPGAs constitute a potential platform as they provide a tunable balance between performance, power consumption and programmability. An overview of the proposed flow is shown here:
 
 ![flow](/images/Proposed_Flow.png)
 
 We first elaborate on details and results of the pruning flow. Then, we explain the utilized method for the FPGA mapping flow, and obtained results for that. Lastly, The future directions and steps that need to be followed for this project are pointed out.

### Pruning
Modern Convolutional neural networks (CNN) typically consist of multiple convolutional and fully connected layers. 
CNNs are computationally and storage intensive due to their large model size. Pruning the weights of the network is one way to reduce the computational requirements as well as the model size of a CNN with minimal loss of accuracy. 
A systematic approach to pruning is to enforce sparsity among the weights by solving a constrained optimization problem during the training of the CNN. 
We will consider alternate direction method of multipliers (ADMM) based approach for pruning the weights of CNN. The detailed describtion of this method is explained [here](https://dl.acm.org/citation.cfm?id=3304076). We briefly describe this approach.


Consider a \\(N\\)-layer CNN with \\(N_{conv}\\) convolutional layers and \\(N_{full}\\) fully connected layers. 
The weights and biases of \\(i^{th}\\) layer is denoted by \\(\mathbf{W}_i\\) and \\(\mathbf{b}_i\\), respectively. 
Let the loss function of the CNN be denoted by \\(f(\{\mathbf{W}_i\}_{i=1}^N,\{\mathbf{b}_i\}_{i=1}^N)\\). 
Training of a CNN involves finding the set of weights and biases which minimizes the loss function over the training set. 
However, in order to enforce sparsity among the weights, we will solve the following optimization problem:
![opt image](/images/opti.png)



where \\(\mathcal{S}_i\\) are known sets which will incorporate sparsity among the weights of the neural network. 
Two different types of pruning can be considered:

- **Non-structured pruning:** Here there are no constraint on what weights will be dropped. This is considered in [this paper](https://arxiv.org/abs/1804.03294) where \\(\mathcal{S}_i = \{\mathbf{W}_i: \text{cardinality}(\mathbf{W}_i) \leq l_i \}\\). 
- **Structured Pruning:** Here the weights being dropped have some additional spatial structure. This is considered in [this paper](https://arxiv.org/abs/1807.11091) where three types of sparsity structure can be imposed:
    - **Filter pruning:** Here we zero out some of the filters entirely from the convolutional layer weights. 
    - **Channel pruning:** Here we zero out some of the channels entirely from all the filters of a convolutional layer. 
    - **Shape-wise pruning:** Here we zero out some specific shape from each of the filter of a convolutional layer. 
    
An illustration of structured pruning (specifically filter pruning, channel pruning, and shape-wise pruning) is depicted in the following figure:  
![Pruning_3figs image](/images/pruning_3figs.PNG)

We can convert the above optimization into the following optimization:
![opt image](/images/admm_opti.png)
where \\(g_i( \mathbf{W}_i)= 0\\) if \\(\mathbf{W}_i \in \mathcal{S}_i\\); otherwise \\(g_i( \mathbf{W}_i)= \infty\\).

The above optimization can be solved by following the ADMM technique which decomposes the main problem into two sub-problems and solve them separately and 
update parameters \\(\{\mathbf{W}_i\}\\)'s, \\(\{\mathbf{Z}_i\}\\)'s, and Lagrangian coefficients sequentially. In other words, it first solves the first subproblem with respect 
to \\(\{\mathbf{W}_i\}\\)'s while fixing \\(\{\mathbf{Z}_i\}\\)'s and Lagrangian coefficients. Then, it solves the second sub-problem with respect to \\(\{\mathbf{Z}_i\}\\)'s 
while fixing new values of \\(\{\mathbf{W}_i\}\\)'s obtained in the previous sub-problem, and Lagrangian coefficients. Finally, it updates the Lagrangian coefficients
by having the new values of \\(\{\mathbf{W}_i\}\\)'s and \\(\{\mathbf{Z}_i\}\\)'s.

By considering the above ADMM approach, we can solve the constrained optimization problem so as to systematically train a CNN 
while simultaneously enforcing the sparsity constraints among the weights.




### Results on Pruning
We performed experiments for structural pruning on **LeNet5 network on MNIST** dataset. LeNet5 network consists of 2 convolutional layers and 3 fully connected layers. More details on the convolutional
layers are presented in the following table:

| Conv Layer | No. of filters | No. of channels |
| ------ | ------ |------|
| conv1 | 6 | 5 |
| conv2 | 16 | 5 | 

We focussed only on the structured pruning for this set of experiments and in particular we applied filter pruning for each of the convolutional layers.
We define the pruning ratio as the fraction of the filters which will be removed(pruned) from the network from each convolutional layer.
We set the following setting of pruning ratio:
1. **Setting 1** - Pruning ratio = 0.66 for convolutional layer 1 (i.e. two-third of the filters will be pruned from convolutional layer 1) while Pruning ratio = 14/16 for convolutional layer 2. Thus, we only keep two non-zero filters for each convolutional layer. 

In order to further improve the accuracy, we aimed to consider other potential loss functions. The intuition behind this was to use information-theory related loss
functions such as KL divergence to improve the result compared to cross-entropy case. 
We presented the accuracy of different loss functions: cross entropy, negative log likelihood, and KL divergence. The following table 
lists the test accuracy of the baseline (unpruned) network and the pruned network obtained after the ADMM training:

|  | Cross Entropy (used [here](https://arxiv.org/abs/1807.11091)) | Negative log likelihood (Ours) | Kullback–Leibler divergence (Ours) |
| ------ | ------ | ------ | ------ |
| Baseline | 98.11% | 98.11% | 98.64% |
| Setting 1 | 97.72% | 98.0% | 98.45% |

As one can easily see, the scheme with Kullback–Leibler divergence loss functions considerably improves the accuracy of the prunned network. Also, 
it can be seen that the test accuracy obtained after pruning the network is almost similar to the baseline test, for all loss functions we tried above. 

Next, we conducted experiments for structural pruning on **VGG 11 network on CIFAR-10** dataset. VGG 11 network consists of 8 convolutional layers and 3 fully connected layers. Following are the details of the convolutional
layers:

| Conv Layer | No. of filters | No. of channels |
| ------ | ------ |------|
| conv1 | 64 | 3 |
| conv2 | 128 | 64 | 
| conv3 | 256 | 128 |
| conv4 | 256 | 256 | 
| conv5 | 512 | 256 | 
| conv6 | 512 | 512 | 
| conv7 | 512 | 512 | 
| conv8 | 512 | 512 | 

We focused only on the structured pruning for this case and in particular we did filter pruning for each of the convolutional layers.
We tried the following two settings of pruning ratio:
1. **Setting 1** - Pruning ratio = 0.5 for each convolutional layer i.e. 50% of the filters will be pruned from each convolutional layer.
2. **Setting 2** - Pruning ratio = 0.5 for the first 3 conv layers and Pruning ratio = 0.75 for the remaining 5 conv layers.

We also tried two loss functions: cross entropy and negative log likelihood for both experiments. The following table 
lists the test accuracy of the baseline (unpruned) network and the pruned network obtained after the ADMM training:

|  | Cross Entropy (used [here](https://arxiv.org/abs/1807.11091)) | Negative log likelihood (Ours) |
| ------ | ------ | ------ |
| Baseline | 86.22% | 88.67% |
| Setting 1 | 84.29% | 88.24% |
| Setting 2 | 83.38% | 87.03% |

It can be seen that the test accuracy obtained after pruning the network is almost similar to the baseline test accuracy for both loss functions. In particular, for negative log likelihood loss, the accuracy loss after pruning is only slightly lower compared to the baseline accuracy. Therefore, we again observe that the scheme with negative log likelihood loss functions considerably improves the accuracy of the prunned network compared to using cross entropy loss function.

One interesting observation on the histogram of weights of convolutional layers of VGG setting before and after pruning is presented below. 
The distribution of weights of the first four convolutional layers of VGG11 before pruning are as follows:

![before pruning image](/images/unprune_vgg.png)

However, after performing pruning based on the ADMM optimization technique, the distribution of weights of the first four convolutional layers of VGG11 
will be as follows:

![after pruning image](/images/prune_vgg.png)

As one can easily see, most of of the weights of the convolutonal layers are set to zero.


All our experiments were conducted using PyTorch. the details of training is presented below:
![pipeline image](/images/Screen_Shot_2019-12-05_at_8.55.53_PM.png)
1. **Pre-training** - We first trained the VGG network to establish the baseline test accuracy. We did a training-validation split
by splitting 10% of training data as validation data and trained the network for around 100 epochs.
2. **ADMM-training** - We then re-trained the network using the ADMM based training approach. We initialized the weights of the 
network by using the weights of the pre-trained model. We set \\(\rho = 1.5e^{-3}\\) for each convolutional layer.
We did 5 iterations of ADMM steps and for each ADMM step we trained the network for 20 epochs. 
3. **Masked re-training** - The weights obtained after step 2 do not strictly obey the sparsity constraints. Thus, we enforce the sparsity constraints by forcing a number of weights to zero. In particular, for each layer we compute the frobenius norm of the weights of each filter and keep top k filters (w.r.t frobenius norm) where k is the maximum number of non-zero filters allowed in the layer.
The rest of the filter weights are set to zero. Finally, the network is then re-trained without updating the zeroed out weights in order to improve the accuracy.


### Mapping to FPGA
We have started with a toolflow called [DNNWeaver v2.0](http://prism.sejong.ac.kr/dossa-1/dossa_papers/dossa-1_paper01.pdf) for mapping a given neural network on the target FPGA. DNNWeaver attempts to generate a synthesizable accelerator for a given (DNN, FPGA) pair from a high-level specification. To be more specific, in DNNWeaver v2.0, DNNs are first expressed as graphs, where nodes are operations(layers) like convolution, and edges are tensors. Then, the graph is compiled to generate binary instructions for the FPGA accelerator object which is defined separately using a logic synthesis tool like Xilinx Vivado Design Suite. Then, the compiled DNN graph is run on the FPGA. 
An overview of the toolflow is shown in the figure below:

![DNN overview](/images/DNN_overview.JPG)

In the next section, we first quantify the advantages of structural pruning over non-structural pruning for the hardware accelerator. After that, we will briefly introduce the hardware components of an accelerator, followed by the optimization technique added to the current toolflow for finding a better solution in the design space. 

### Structured vs Non-structured Pruning 
Compressing the data cannot be directly translated to improvements in hardware implementation. One solution is to encode the sparse weights matrices. Encoding the sparse weights provides an architecture implemented on FPGA an opportunity to reduce the amount of data that must be moved throughout the memory hierarchy. It also reduces the data footprint, which allows larger matrices to be held a given size storage structure. There exists encoding schemes that are more suitable for performing efficient operations. two commonly used examples that we will explore during next weeks are listed as below.
1. **Compressed Sparse Row (CSR)**. The sparse matrix is represented using three one-dimensional arrays for the non-zero values, the extents of the rows, and the column indices.
2. **Compressed Sparse Column (CSC)**. The same as the Compressed Sparse Row method except the column indices are compressed and read first before the row indices.
These methods are used for encoding the unstructured weight pruning and need extra hardware for endcoding and decoding data while structured weight pruning can be handled by compiler at no extra hardware cost. In the following, its reason will shortly discussed.

The CONV operations in (one layer of) DNNs are commonly transformed to matrix multiplications by converting weight tensors and feature map tensors to matrices, named general matrix multiplication or GEMM, as shown below.
![GEMM](/images/GEMM.PNG)

filter pruning corresponds to reducing one row while Filter shape (channel) pruning corresponds to reducing one column (columns), and thus is also termed column pruning. The three structured pruning techniques, along with their combinations, will reduce the dimensions in GEMM while maintaining a full matrix format without extra indices for locating non-zero weights.

### Accelerator Components for FPGA-based Implementation of CNNs
CNN accelerator design on FPGA consists of several major components, which are processing elements (PEs), on-chip buffer, external memory, and on-/off-chip interconnect. A PE is the basic computation unit for convolution. All data for processing are stored in the external memory. Due to on-chip resource limitation, data are first cached in on-chip buffers before being fed to PEs. Double buffering technique is used to cover computation time with data transfer time. The on-chip interconnect is dedicated for data communication between PEs and on-chip buffer banks. The implementation of our CNN accelerator is as follows:


![Design Block](/images/Design_Block.png)

### Design Space Exploration for FPGA-based Implementation of CNNs
In this section, we start our optimization from the code below and present our methodology for exploring the design space to achieve an efficient design. 

![4 loops](/images/loop.JPG)

For this purpose, we unrolled some of the for loops showed above. This is called tiling and those added loops called a computational tile, which will be processed before storing the final results on the off-chip memory. Here is an example of this tiling: 

![tile loops](/images/tileloops.JPG)

![tile loops 2](/images/tileloops2.JPG)


### Results on FPGA mapping
Using Xilinx Vivado Design Suite, we were able to synthesize the hardware using the clock frequency of 100MHz without any timing failure. For the target Board we used  Virtex UltraScale+ VCU118 Evaluation Platform. The mapped hardware architecture took around 110,000 LUT blocks, which is around 10% of all LUT resources of the studied Board. A screenshot of the synthesized hardware using Vivado is shown here:

![Vivado Results](/images/vivado.jpg)

We mapped two CNN models, which are LeNet-5 and Tiny YOLO, on our target FPGA Borad. For each of these two CNN models, the results for the inference lateny and the amount of hardware energy consumption during the inference, alongside the number of generated binary instructions using DNNWeaver v2.0 are shown below:

| Model | Latency (ms) | Energy (mJ) | No. of Generated Binary Instructions |
| ------ | ------ |------| ------ |
| LeNet-5 | 1.09 | 9.71 | 257 |
| Tiny YOLO | 6.08 | 54.18 | 1433 |

The energy consumption for each model in the table above is obtained from the power consumption of the synthesized hardware (which was 8.91 W), multiplied by the inference time of that model. Compared to GTX 650Ti, which is a middle-tier GPU, obtained results show an average 3.2X improvement in terms of performance-per-Watt.

### Future Work
The next step for this project would be merging the pruning flow with the FPGA tool flow (i.e., description of the pruned CNN would be the input to the FPGA mapping flow). For this purpose, memory management in the underlying hardware should be changed in a way that it supports the sparsity in the CNN model. We can also investigate increasing the clock frequency of the hardware (which is currently clocked at 100MHz), and incorporating more advanced gradient-based techniques for improving the pruning flow, such as second-order gradient based algorithms. The ultimate goal for this project is automatically generating a synthesizable accelerator for a given (CNN, FPGA) pair from a high-level specification.


